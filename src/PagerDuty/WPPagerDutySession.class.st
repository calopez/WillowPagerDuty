"
I provide global state for the application and deal with login/logout as well as determining which content is shown in the application frame.
"
Class {
	#name : #WPPagerDutySession,
	#superclass : #WillowSession,
	#instVars : [
		'applicationContext',
		'identifiedContentView',
		'contentView',
		'scheduleView',
		'loginView'
	],
	#category : #PagerDuty
}

{ #category : #accessing }
WPPagerDutySession >> applicationContext [

	^ applicationContext
]

{ #category : #Controlling }
WPPagerDutySession >> cookiePath [
	^WPPagerDutyApp handlerName 
]

{ #category : #accessing }
WPPagerDutySession >> deleteToken [
		(self requestContext request cookieAt: 'token') ifNotNil: [ :c | self requestContext response deleteCookie: c ]
]

{ #category : #accessing }
WPPagerDutySession >> hasToken [
	^self applicationContext token notNil
]

{ #category : #Controlling }
WPPagerDutySession >> identifiedContentView [ 
	^identifiedContentView 
]

{ #category : #Controlling }
WPPagerDutySession >> initialize [

	super initialize.
	
	self setContentTo: [ :canvas | canvas paragraph: 'loading...' ].

	identifiedContentView := IdentifiedWebView
		forDivNamed: 'content'
		containing: [ :canvas | canvas render: contentView ]
]

{ #category : #Controlling }
WPPagerDutySession >> initializeContentView [
	scheduleView := WPPagerDutyUserScheduleView new.
	loginView := WPPagerDutyLoginView new.

	self
		setContentTo:
			(self hasToken
				ifTrue: [ scheduleView ]
				ifFalse: [ loginView ])
]

{ #category : #Controlling }
WPPagerDutySession >> loginWith: aTokenString [
	self
		storeToken: aTokenString;
		setContentTo: WPPagerDutyUserScheduleView new
]

{ #category : #Controlling }
WPPagerDutySession >> logout [
	self
		deleteToken;
		setContentTo: loginView 
]

{ #category : #accessing }
WPPagerDutySession >> retrieveToken [
	^ self
		token:
			((self requestContext request cookieAt: 'token')
				ifNil: [ nil ]
				ifNotNil: [ :c | c value ])
]

{ #category : #Controlling }
WPPagerDutySession >> setContentTo: aWebView [
	
	contentView := aWebView 
]

{ #category : #Controlling }
WPPagerDutySession >> startUpApplicationContextFor: app [
	super startUpApplicationContextFor: app.
	applicationContext := WPPagerDutyContext new.
	
	self 
		retrieveToken;
		initializeContentView
]

{ #category : #accessing }
WPPagerDutySession >> storeToken: aTokenString [
	self requestContext response addCookie: 
		(self requestContext newCookie
				path:'/', self cookiePath;
				expireIn: 7 days;
				key: 'token';
				value: aTokenString).
	self token: aTokenString
]

{ #category : #accessing }
WPPagerDutySession >> token: aTokenString [
	^self applicationContext token: aTokenString
]
