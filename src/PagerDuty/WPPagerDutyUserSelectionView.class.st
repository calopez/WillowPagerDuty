Class {
	#name : #WPPagerDutyUserSelectionView,
	#superclass : #WPWebView,
	#instVars : [
		'datePickerFrom',
		'datePickerTo',
		'usersListBox'
	],
	#category : #PagerDuty
}

{ #category : #initialization }
WPPagerDutyUserSelectionView >> initialize [
	super initialize.
	
	self
		initializeUsersListBox;
		initializeDatePickers
]

{ #category : #initialization }
WPPagerDutyUserSelectionView >> initializeDatePickers [
	| configureCmd |
	
	configureCmd := BootstrapDatepickerCommand
		configuredBy: [ :field | 
			field
				autoclose: true;
				format: 'dd/mm/yyyy' ].

	datePickerFrom := self componentSupplier
		dateFieldApplying: [ :cmdBuilder | (cmdBuilder setPlaceholderTo: self context startDate ddmmyyyy) + configureCmd ].
		
	datePickerTo := self componentSupplier
		dateFieldApplying: [ :cmdBuilder | (cmdBuilder setPlaceholderTo: self context endDate ddmmyyyy) + configureCmd ].

	datePickerFrom onTrigger
		serializeIt;
		evaluate: [ self context startDate: (Date readFrom: datePickerFrom contents pattern: 'dd/mm/yyyy') ].

	datePickerTo onTrigger
		serializeIt;
		evaluate: [ self context endDate: (Date readFrom: datePickerTo contents pattern: 'dd/mm/yyyy') ]
]

{ #category : #initialization }
WPPagerDutyUserSelectionView >> initializeUsersListBox [
	usersListBox := self componentSupplier
		singleSelectionListBoxDisplayingAtOnce: 20
		applying: [ :listBox | listBox labelItemsWith: [ :user | user name ] ].
		
	self onTrigger
		serializeIt;
		evaluate: [ usersListBox
				withCurrentSelectionDo: [ :selectedItem | self context developer: selectedItem name] ]
]

{ #category : #configuring }
WPPagerDutyUserSelectionView >> onTrigger [

	^ usersListBox onTrigger 
]

{ #category : #rendering }
WPPagerDutyUserSelectionView >> renderContentOn: html [
	self
		whileEvaluating: [ usersListBox allowAnyOf: self context users ]
		render: [ :delayedCanvas | 
			delayedCanvas
				render:
					(self componentSupplier gridBuilder
						columnsPerRow: 1;
						addContent: [ :canvas | canvas heading: 'User Report' ];
						addContent: [ :canvas | canvas paragraph: 'Start Date:' ];
						addContent: datePickerFrom;
						addContent: [ :canvas | canvas paragraph: 'End Date:' ];
						addContent: datePickerTo;
						addContent: [ :canvas | canvas break ];
						addContent: usersListBox;
						build) ]
		on: html
]
