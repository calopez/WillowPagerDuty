Class {
	#name : #WPPagerDutyUserDetailsView,
	#superclass : #WPWebView,
	#instVars : [
		'user',
		'number',
		'table'
	],
	#category : #PagerDuty
}

{ #category : #rendering }
WPPagerDutyUserDetailsView >> initialize [
	| durationBlock numberPrinter |
	super initialize.

	durationBlock := [ :d | ((DateAndTime fromString: d end) - (DateAndTime fromString: d start)) asHours ].
	numberPrinter := GRNumberPrinter new precision: 2; yourself.
	
	table := TableWebViewBuilder new
		addColumnTitled: 'Start' rendering: [ :data | data start ];
		addColumnTitled: 'End' rendering: [ :data | data end ];
		addColumnTitled: 'Hours'
			rendering: [ :data | numberPrinter print: (durationBlock value: data) ]
			applyingToCells: [  ]
			applyingToHeading: [  ]
			summarizedWith: [ :rows | numberPrinter print: (rows sumNumbers: [ :row | durationBlock value: row ]) ]
			applyingToFooter: [  ];
		addColumnTitled: 'Days'
			rendering: [ :data | numberPrinter print: (durationBlock value: data) / 24 ]
			applyingToCells: [  ]
			applyingToHeading: [  ]
			summarizedWith: [ :rows | numberPrinter print: (rows sumNumbers: [ :row | durationBlock value: row ]) / 24 ]
			applyingToFooter: [  ];
		buildApplying: [ :theTable | theTable addClass bootstrap table ]
]

{ #category : #rendering }
WPPagerDutyUserDetailsView >> renderContentOn: html [
	self
		whileEvaluating: [ table changeContentsTo: self context schedule ]
		render: [ :canvas | 
			canvas
				render:
					(self componentSupplier gridBuilder
						columnsPerRow: 1;
						addContent: [ :gridCanvas | 
							gridCanvas
								heading: (self context developer ifNil: [ 'N/A' ] ifNotNil: [ :d | d , '''s Hours' ]) ];
						addContent: table) build ]
		on: html
]
