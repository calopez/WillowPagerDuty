"
I am the main content of the app that lets you select users and view their schedule hours
"
Class {
	#name : #WPPagerDutyUserScheduleView,
	#superclass : #WPWebView,
	#instVars : [
		'contents'
	],
	#category : #PagerDuty
}

{ #category : #rendering }
WPPagerDutyUserScheduleView >> initialize [
	| behaviorDetails scopeSelection |

	super initialize.

	behaviorDetails := IdentifiedWebView
		forDivNamed: 'schedule-details'
		containing: WPPagerDutyUserDetailsView new 
		applying: [ :div :constants | 
			div
				setStyleTo: [ :style | 
					style
						maxHeight: 100 vh;
						overflowY: constants css scroll ] ].

	scopeSelection := WPPagerDutyUserSelectionView new.
	scopeSelection onTrigger
		executeOnClient: [ :script :canvas | 
			behaviorDetails identifyIn: canvas.
			script << (canvas jQuery id: behaviorDetails identifier) html: SpinKitTripleBounce new ];
		render: behaviorDetails.

	contents := GenericContainerWebView
		wrapping:
			(self componentSupplier gridBuilder
				columnsWidths: #(3 9);
				addContent: scopeSelection;
				addContent: behaviorDetails;
				build)
		applying: [ :div | ]
]

{ #category : #rendering }
WPPagerDutyUserScheduleView >> renderContentOn: html [
	
	html
		render:contents 
]
