"
I bundle application session variables into a location that makes it easy to copy  them into a new session

(I could be conceivable merged into the session - the verdict is out on this one)
"
Class {
	#name : #WPPagerDutyContext,
	#superclass : #GRObject,
	#instVars : [
		'developer',
		'startDate',
		'endDate',
		'users',
		'schedules',
		'token'
	],
	#category : #PagerDuty
}

{ #category : #accessing }
WPPagerDutyContext >> dateRange [
	^Timespan starting: self startDate ending: self endDate
]

{ #category : #accessing }
WPPagerDutyContext >> dateRange: aTimespan [

	self startDate: aTimespan start asDate.
	self endDate: aTimespan end asDate.
]

{ #category : #accessing }
WPPagerDutyContext >> developer [
	^ developer
]

{ #category : #accessing }
WPPagerDutyContext >> developer: anObject [
	developer := anObject
]

{ #category : #accessing }
WPPagerDutyContext >> endDate [
	^ endDate
]

{ #category : #accessing }
WPPagerDutyContext >> endDate: anObject [
	self resetCache.
	endDate := anObject
]

{ #category : #initialization }
WPPagerDutyContext >> initialize [
	super initialize.

	self
		resetCache;
		dateRange: Month current previous;
		developer: nil;
		token: nil
]

{ #category : #private }
WPPagerDutyContext >> queryPagerDutyAt: aPath applying: aBlock [ 

	| client |
	client := ZnClient new
		url: 'https://api.pagerduty.com', aPath;
		accept: 'application/vnd.pagerduty+json;version=2';
		headerAt: 'Authorization' put: 'Token token=', self token;
		contentReader: [ :entity | 
			(NeoJSONReader on: entity readStream)
				mapClass: NeoJSONObject;
				next ].
			
	aBlock value: client.

	^client get
]

{ #category : #initialization }
WPPagerDutyContext >> resetCache [
	"A very crude cache"
	self schedules: Dictionary new.

	
]

{ #category : #accessing }
WPPagerDutyContext >> schedule [
	^ self developer
		ifNil: [ {} ]
		ifNotNil: [ :devName | schedules at: devName ifAbsentPut: [ self scheduleEntriesForUser: devName ] ]
]

{ #category : #private }
WPPagerDutyContext >> scheduleEntriesForUser: aName [
	| items |
	items := self scheduleIds
		inject: OrderedCollection new
		into: [ :result :id | 
			result
				addAll: (self scheduleEntriesFromId: id);
				yourself ].


	^ items select: [ :i | (i atPathString: 'user.summary') = aName ]
]

{ #category : #private }
WPPagerDutyContext >> scheduleEntriesFromId: scheduleId [
	| scheduleQuery ymdFormat |
	
	ymdFormat := #(3 2 1 $- 1 1 2).

	scheduleQuery := self
		queryPagerDutyAt: '/schedules/' , scheduleId
		applying: [ :client | 
			client
				queryAt: 'timezone' put: 'UTC';
				queryAt: 'since' put: (self startDate printFormat: ymdFormat);
				queryAt: 'until' put: (self endDate + 1 day printFormat: ymdFormat) ].

	^ scheduleQuery atPathString: 'schedule.final_schedule.rendered_schedule_entries'
]

{ #category : #accessing }
WPPagerDutyContext >> scheduleIds [
	"These should really be queried from PageDuty with user token"
	^ #('PWSBPVE' 'PDHPYM9')
]

{ #category : #accessing }
WPPagerDutyContext >> schedules [
	^ schedules
]

{ #category : #accessing }
WPPagerDutyContext >> schedules: anObject [
	schedules := anObject
]

{ #category : #accessing }
WPPagerDutyContext >> startDate [
	^ startDate
]

{ #category : #accessing }
WPPagerDutyContext >> startDate: anObject [
	self resetCache.
	startDate := anObject
]

{ #category : #accessing }
WPPagerDutyContext >> token [
	^ token
]

{ #category : #accessing }
WPPagerDutyContext >> token: anObject [
	token := anObject
]

{ #category : #accessing }
WPPagerDutyContext >> users [
	| result |
	users
		ifNil: [ result := self queryPagerDutyAt: '/users' applying: [ :client |  ].
			users := result users
				select: [ :u | (u teams detect: [ :t | t summary = 'Developers' ] ifNone: [ {} ]) notEmpty ] ].

	^ users
]

{ #category : #accessing }
WPPagerDutyContext >> users: anObject [
	users := anObject
]
