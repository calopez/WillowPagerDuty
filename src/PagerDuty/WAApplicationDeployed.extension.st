Extension { #name : #WAApplicationDeployed }

{ #category : #'*PagerDuty' }
WAApplicationDeployed >> installApplicationFileHandlerFor: aWillowApplication [
	
	WADeploymentAwareFileHandler
		installAsFileHandlerAccordingTo: self
		servedAt: (WAUrl absolute: 'files' relative: aWillowApplication handlerName)
		
]
