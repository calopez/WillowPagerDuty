Class {
	#name : #WPPagerDutyMenuView,
	#superclass : #WPWebView,
	#instVars : [
		'menu'
	],
	#category : #PagerDuty
}

{ #category : #initialization }
WPPagerDutyMenuView >> initialize [
	menu := self componentSupplier dropdownMenuBuilder
		labeled: 'Menu' applying: [ :field | ];
		addActionLabeled: 'Logout'
			executing: [ :link | 
			link onTrigger
				evaluate: [ self session logout ];
				render: self session identifiedContentView ];
		beRightAligned;
		buildApplying: [ :control | control addClass bootstrap navbarRight ]
]

{ #category : #initialization }
WPPagerDutyMenuView >> renderContentOn: aCanvas [
	aCanvas navigation
		class: BootstrapCssStyles navbarDefault, ' ', (BootstrapCssStyles columnMedium: 12);
		with: [ aCanvas render: menu ]
]
