Class {
	#name : #BaselineOfPagerDuty,
	#superclass : #BaselineOf,
	#category : 'BaselineOfPagerDuty'
}

{ #category : #baselines }
BaselineOfPagerDuty >> baseline: spec [

	<baseline>
	spec
		for: #common
		do: [ self setUpDependencies: spec.
			spec
				package: 'PagerDuty'
					with: [ spec requires: #('NeoJSON' 'WillowBootstrap' 'WillowJQueryUI' 'WillowSpinKit') ];
				group: 'default'
					with:
					#('PagerDuty') ]
]

{ #category : #baselines }
BaselineOfPagerDuty >> setUpDependencies: spec [

	spec 
		configuration: 'NeoJSON' 
			with: [
				spec
					className: #ConfigurationOfNeoJSON;
					versionString: #'stable';
					repository: 'http://mc.stfx.eu/Neo' ]. 
	spec
		baseline: 'WillowBootstrap'
			with: [ spec
				repository: 'github://ba-st/Willow-Bootstrap:v8/source';
				loads: #('Deployment') ];
		import: 'WillowBootstrap'.

	spec
		baseline: 'WillowJQueryUI'
			with: [ spec
				repository: 'github://ba-st/Willow-JQueryUI:v7/source';
				loads: #('Deployment') ];
		import: 'WillowJQueryUI'.

	spec
		baseline: 'WillowSpinKit'
			with: [ spec
				repository: 'github://ba-st/Willow-SpinKit:v5/source';
				loads: #('Deployment') ];
		import: 'WillowSpinKit'

]
