"CmdLine script to load project"

| logger src_cache app_name |

logger := FileStream stderr.
app_name := ApplicationSettings at: #name.
logger cr; nextPutAll: 'Starting ', app_name, ' Load Script...'.

"Create a stub TestCase (if needed due to a minimal image) for packages not minimally defined"
Smalltalk at: #TestCase ifAbsent: [
    logger cr; nextPutAll: '>Creating Dummy TestCase'.
    Object subclass: #TestCase ].

logger cr; nextPutAll: '>Configure Load caches...'.
IceRepository reset.
Iceberg 
    enableMetacelloIntegration: true;
    remoteTypeSelector: #httpsUrl.

src_cache := '../pharo-cache/' asFileReference.

IceRepository
    shareRepositoriesBetweenImages: true;
    sharedRepositoriesLocation: src_cache.

MCCacheRepository cacheDirectory: src_cache.
MCGitHubRepository cacheDirectory: src_cache.

logger cr; nextPutAll: '>Loading Projects...'; cr.

Metacello new
    baseline: app_name;
    repository: 'tonel://../src';
    load.

logger cr; cr; nextPutAll: '<Loading Complete.'.

3 timesRepeat: [
        Smalltalk garbageCollect.
        Smalltalk cleanOutUndeclared.
        Smalltalk fixObsoleteReferences].

logger cr; nextPutAll: 'Finished Load Script.'; cr; cr.