#!/bin/sh
# Shell script to build a clean Pharo deployable image based in Gitlab from a docker imager

set -veu

rm -rf build deploy
mkdir build deploy
mkdir -p $PHARO_CACHE_DIR

echo -e "\e[1m\nCopying Pharo image and project scripts...\e[0m"
cp $PHARO_DIR/Pharo.* build/
cp scripts/*.s? build
cp scripts/*.ini build
cp scripts/*.nginx build
cd build

ls -al

echo -e "\e[1m\nTesting Pharo image (note: ignore any: pthread_setschedparam failed errors)...\e[0m"
pharo Pharo.image printVersion

if [ "$STRIP_IMAGE?false" = true ]; then
    echo -e "\e[1m\nStripping Pharo image...\e[0m";
    pharo Pharo.image --no-default-preferences --save --quit st minimal.st;
    ls -lh Pharo.*
fi

echo -e "\e[1m\nLoading project packages...\e[0m"
pharo Pharo.image --no-default-preferences --save --quit st settings.st loadProject.st config.st \
    "{'$PROJECT_NAME'. '$DOMAIN_LOGIN'}" 2>&1 | tee LoadProject.log

echo -e "\e[1m\nVerifying project load history...\e[0m"
if (grep -f ../scripts/errorPatterns.txt LoadProject.log | grep -v -f ../scripts/errorPatternsIgnored.txt); then
    echo -e "\e[91m\nERRORS detected in package load!\e[0m";
    exit 1;
fi

cp Pharo.image $PROJECT_NAME.image
cp Pharo.changes $PROJECT_NAME.changes
ls -alh

echo -e "\e[1m\nCreating project deployment zip...\e[0m"
# for ultra small could also -x \*.sources \*.changes
#       */libgit2.* */libSDL2* */B3DAccelerator* */JPEGRead* */vm-sound* */vm-display* \
# and: zip -uR ../deploy/$PROJECT_NAME.zip *-null.so

zip ../deploy/$PROJECT_NAME.zip $PROJECT_NAME.* run.st -x \*.log tmp/\*

cd ../deploy
ls -alh

SIZE=$(perl -e "printf(\"%.2f\", $(unzip -l $PROJECT_NAME.zip | tail -1 | xargs | cut -d' ' -f1) / 1024 / 1024)")
echo -e "\e[1m\n\nFinal assets Total decompressed size: $SIZE mb\n\e[0m"
