"Bind some application settings"

| logger configParams |

logger := FileStream stderr.
logger cr; nextPutAll: 'Initialising ApplicationSettings...'.

"Expect image to be called with params as a last arg array"
configParams := Array readFrom: Smalltalk arguments last.

(Smalltalk at: #ApplicationSettings put: Dictionary new)
	at: #name put: (configParams at: 1);
    at: #key put: (configParams at: 2).

logger cr; nextPutAll: 'OK'; cr.